//090318@LorenzoPedroza
package BeginnerStuff;

public class ForAndWhileLoops {
    public static void main(String[] args) {
        //for loop
        for (int i = 0; i < 10; i++) //Single line does not require curly brackets
            System.out.println("Counting " + i);

        //While Loop
        int i = 0;
        while (i < 5){
            System.out.println("Counting.. " + i);
            i++;
        }
    }
}
