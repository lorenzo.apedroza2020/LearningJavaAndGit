package BeginnerStuff;

import java.util.ArrayList;
import java.util.Arrays;

public class BoscoTechStudent extends Student implements STEM{

    private static final ArrayList<String> DBTIClasses = new ArrayList<>(Arrays.asList("English", "Math", "Science", "History", "Theology", "Foreign Language", "Tech"));
    private final String idNumber;

    //https://stackoverflow.com/questions/10963775/cannot-reference-x-before-supertype-constructor-has-been-called-where-x-is-a
    //https://stackoverflow.com/questions/14021827/extends-class-and-implements-interface-in-java

    BoscoTechStudent(String name, Gender sex, int age, String idNumber) {
        super(name, sex, age, DBTIClasses);
        this.idNumber = idNumber;
    }

    public String doesSTEM() {
        return "\n" + name + " does Science Technology Engineering and Math";
    }
}
