package BeginnerStuff;

import java.util.ArrayList;

public class Student {

    protected final Gender sex;
    protected final String name;
    protected final int age;
    protected ArrayList<String> classes;

    Student(String name, Gender sex, int age, ArrayList<String> classes) {
        this.sex = sex;
        this.name = name;
        this.age = age;
        this.classes = classes;
    }

    void printAllClasses(){
        System.out.print("\n" + name + "'s classes are: \n");
        for (String StudentsClasses:classes)
            System.out.println(StudentsClasses);
    }

    Boolean hasClass(String searchingFor){
        return classes.contains(searchingFor);
    }

    void printSex(){
        System.out.println("\n" + name + "'s sex is " + sex);
    }

    int getAge(){
        return age;
    }

}
