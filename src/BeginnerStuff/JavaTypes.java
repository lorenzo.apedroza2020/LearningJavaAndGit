
package BeginnerStuff;

public class JavaTypes {
    /**
     * Practicing Java Doc Comments. Made by Lorenzo Pedroza.
     */
    public static void main(String[] args) {
        //Primitive Types
        int age = 10;
        char b = 'b';
        double temp = 97.667;
        //Byte - small type
        byte myByte = -128; //max can store values from -128 to 127

        //Short 2x size of byte (32,768 and a maximum value of 32,767 (inclusive))
        short myShort = 32767;

        //Float 1.34, 3.14....
        float piNum = 3.14f; //float needs you to add f at end; can use fractions

        //Double - bigger than float, fractions
        double myDouble = 24.50;

        boolean isOn = true;

        //Included types in Java
        String myString = "Hello, I am a string";

        //Concatenation (notice you can concatenate many different primitive types and strings in Java)
        System.out.println("Hello, boy " + b + " is " + age + " years old and has a temp of " + temp);
        System.out.println("A byte: " + myByte + "\nA short: " + myShort + "\nA float: " + piNum + "\nA double: " + myDouble+ "\nA string: " + myString + "\nA Boolean: " + isOn);



    }
}
